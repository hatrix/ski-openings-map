from datetime import datetime, timedelta

import os
import requests

def get_stations(base_url, country):
    # Get raw data from french website with french stations
    r = requests.get('{}/{}'.format(base_url, country))

    # Split on links
    stations = r.text.split('a class="location')
    stations = [s.split('"\ntitle')[0] for s in stations]
    stations = [s.split('href="')[-1] for s in stations]
    stations = stations[1:] # first link is a google appstore redirect
    
    stations_links = ['{}{}'.format(base_url, s) for s in stations]

    return stations_links


def get_station_details(url):
    url = '{}/schneebericht/'.format(url)
    r = requests.get(url)
    text = r.text

    km = None
    total_km = None
    slopes = None
    total_slopes = None

    opened_km = text.split('<dt>Pistes ouvertes</dt>')
    if len(opened_km) != 1:
        tmp = opened_km[1].split('<dd class="big">\n')
        first = tmp[1].split("<")[0].strip()
        first_total = tmp[1].split('</span><div title="Pistes ouvertes"')[0].split('de ')[1]

        if "km" in first:
            km = first.split("km")[0].strip()
            total_km = first_total.split("km")[0].strip()    
        else:
            slopes = first
            total_slopes = first_total

        if len(tmp) > 2:
            second = tmp[2].split('<')[0].strip()
            second_total = tmp[2].split('</span><div title="Pistes ouvertes"')[0].split('de ')[1]

            if "km" in second:
                km = second.split("km")[0].strip()
                total_km = second_total.split("km")[0].strip()    
            else:
                slopes = second
                total_slopes = second_total

    try:
        int(total_slopes)
    except:
        total_slopes = None

    return {'opened_km': km, 
            "total_km": total_km,
            "opened_slopes": slopes,
            "total_slopes": total_slopes
            }


def get_station_price(url, country):
    url = '{}/preise/'.format(url)
    r = requests.get(url)
    text = r.text

    # Some stations accept both euros and francs, just test everything…
    currencies = ('<span class="currency">CHF</span>', 
                  '€')

    if '1 Jour' not in text:
        return None

    text = text.split('1 Jour\n')[1]
    text = text.split('</td>')[0]

    for currency in currencies:
        try:
            text = text.split(currency)[1]
            break
        except:
            continue
        return None

    text = text.split('\n')[0].strip()

    return text

def get_station_infos_if_open(url, country):
    # Get station's info:
    #  opening date
    #  number of open slopes / total
    #  number of open km / total
    r = requests.get(url)
    text = r.text

    # Opening
    opening = text.split('Saison</dt>\n<dd><span class="important">')
    opening = opening[1].split("</dd>")[0].strip()
    if opening != '-':
        opening = datetime.strptime(opening.split('-')[0].strip(), '%d.%m.%Y')
    else:
        opening = None

    # Name of station
    name = text.split('title="')[9].split('">')[0]

    # Total km
    total_km = text.split('<dt>total</dt>\n<dd class="big">')[1]
    total_km = total_km.split("</dd>")[0]
    
    # Geolocation
    geo = text.split('meta name="ICBM" content="')
    if len(geo) == 1:
        geo = None
    else:
        geo = geo[1].split('"/>')[0].split(', ')
    
    now = datetime.now()
    delta = timedelta((12 - now.weekday()) % 7)
    if opening and opening <= (now + delta):
        currency = {'frankreich': '€', 'schweiz': 'fr'}

        details = get_station_details(url)
        details['opening'] = opening
        details['geo'] = geo
        details['name'] = name
        details['price'] = get_station_price(url, country)
        details['currency'] = currency[country]

        # If no data of opened slopes / km, assume it's closed
        #if details['opened_km'] is None and details['opened_slopes'] is None:
        #    return None

        return details

    return None


def generate_address_points(stations):
    def get_str_details(details):
        opening = details['opening'].strftime("%Y-%m-%d")
        
        km = "Opened km: {} / {}".format(details['opened_km'], 
                                         details['total_km'])
        slopes = "Opened slopes: {} / {}".format(details['opened_slopes'],
                                                 details['total_slopes'])
        if details['price'] is not None:
            price = "Price: {} {}".format(details['price'], details['currency'])
        else:
            price = "Price: None"

        opening = "Opening: {}".format(opening)

        ret = km + '<br>' + slopes + '<br>' + opening + '<br>' + price
        return ret

    points = "var addressPoints=["
    for station in stations:
        geo = '{},{}'.format(station['geo'][0], station['geo'][1])
        str_s = get_str_details(station)
        tmp = '[{},"{}","{}"],'.format(geo, station['name'], str_s)
        points += tmp

    points = points[:-1]
    points += ']'

    f = open(os.path.join('points', 'address_points.js'), 'w')
    f.write(points)
    f.close()

if __name__ == "__main__":
    url = "https://www.bergfex.fr"
    countries = ("frankreich", "schweiz")
    countries = countries[::-1]
    
    all_details = []
    for country in countries:
        stations_links = get_stations(url, country)

        for i, link in enumerate(stations_links):
            details = get_station_infos_if_open(link, country)
            if details:
                all_details.append(details)
                print("Storing station {}".format(details['name']))

    generate_address_points(all_details)
    print('Written {} stations'.format(len(all_details)))
